import { NextPage } from 'next';

import Layout from '../components/Layout';
//import useTranslation from '../lib/useTranslation';
import useElveTranslation from '../lib/useTranslation';
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
const About: NextPage = () => {
  //const { t, locale } = useTranslation();
  const { t } = useTranslation('common');
  //const {  locale } = useElveTranslation();
  //const tutorialSlug =
   // locale == 'pt' ? '/post/next-multilingue' : 'en/post/next-intl/';
   const router = useRouter();
  return (
    <Layout title={t('about')} className="about" >
      <section className="page-content">
        <h1>{t('about')}</h1>
        <div className="page-text">
        {t('about')}
          
        </div>
      </section>
    </Layout>
  );
};
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})
export default About;
